package com.program.search;

import java.util.Scanner;

public class BinarySearch {

	public static void main(String[] args) {
		int[] a = { 1, 6, 8, 9, 11, 13, 90 };
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the value to search");
		int value = sc.nextInt();
		boolean result = binarySearch(a, value, 0, a.length - 1);
		if (result)
			System.out.println("Found");
		else
			System.out.println("Not Found");
		sc.close();
	}

	private static boolean binarySearch(int[] a, int value, int start, int end) {
		if (start > end)
			return false;

		int middle = a[(start + end) / 2];

		if (value < middle) {
			return binarySearch(a, value, start, ((start + end) / 2) - 1);
		} else if (value > middle) {
			return binarySearch(a, value, ((start + end) / 2) + 1, end);
		} else {
			return true;
		}
	}

}
