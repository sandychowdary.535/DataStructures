package com.program.search;

import java.util.Scanner;

public class LinearSearch {

	public static void main(String[] args) {
		int[] a = { 5, 1, 6, 2, 4, 3 };
		boolean notFound = true;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the value to search");
		int value = sc.nextInt();
		for (int i = 0; i < a.length; i++) {
			if (value == a[i]) {
				System.out.println("found");
				notFound = false;
				break;
			}
		}
		if (notFound) {
			System.out.println("Not Found");
		}

		sc.close();

	}

}
