package com.program.sorts;

public class SelectionSort {

	public static void main(String[] args) {
		int[] a = { 5, 1, 6, 2, 4, 3 };
		for (int i = 0; i < a.length - 1; i++) {
			int min = i; // setting min as i
			for (int j = i + 1; j < a.length; j++) {
				if (a[j] < a[min]) // if element at j is less than element at
									// min position
				{
					min = j; // then set min as j
				}
			}
			int temp = a[i];
			a[i] = a[min];
			a[min] = temp;
		}
		System.out.print("Sorted list is : ");
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
	}

}
