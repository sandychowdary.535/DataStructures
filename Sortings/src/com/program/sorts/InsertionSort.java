package com.program.sorts;

public class InsertionSort {
	public static void sort(Integer a[])
	{
		for (int i = 1; i < a.length; i++) {
			int key = a[i];
			int j = i - 1;
			while (j >= 0 && key < a[j]) {
				a[j + 1] = a[j];
				j--;
			}
			a[j + 1] = key;
		}
	}

	public static void main(String[] args) {
		Integer[] a = { 5, 1, 6, 2, 4, 3 };
		InsertionSort.sort(a);
		System.out.print("Sorted list is : ");
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
	}

}
