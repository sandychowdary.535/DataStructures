package com.program.sorts;

public class BubbleSort {
	public static void main(String[] args) {
		int[] a = { 5, 1, 6, 2, 4, 3 };
		boolean flag = true;
		for (int i = 0; i < a.length && flag; i++) {
			for (int j = 0; j < a.length - i - 1; j++) {
				flag = false; // taking a flag variable
				if (a[j] > a[j + 1]) {
					int temp = a[j];
					a[j] = a[j + 1];
					a[j + 1] = temp;
					flag = true; // setting flag as 1, if swapping occurs
				}
			}
		}
		System.out.print("Sorted list is : ");
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
	}
}
