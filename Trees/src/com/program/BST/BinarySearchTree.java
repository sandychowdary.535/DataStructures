package com.program.BST;

import java.util.Scanner;

public class BinarySearchTree {
	static Node root;

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		/* Creating object of class linkedList */
		BinarySearchTree tree = new BinarySearchTree();
		System.out.println("Singly Linked List Test\n");
		char ch;
		/* Perform list operations */
		do {
			System.out.println("\n Singly Linked List Operations \n");
			System.out.println("1. insert an element;");
			System.out.println("2. inorder traversing;");
			System.out.println("3. preorder traversing;");
			System.out.println("4. postorder traversing;");
			System.out.println("5. smallest element;");
			System.out.println("6. largest element;");
			System.out.println("7. search an element;");
			int choice = scan.nextInt();
			switch (choice) {
			case 1:
				System.out.println("Enter integer element to insert");
				tree.insertANode(new Integer(scan.nextInt()));
				break;
			case 2:
				System.out.println("Inorder traversing");
				tree.inOrder(root);
				break;
			case 3:
				System.out.println("Preorder traversing");
				tree.preOrder(root);
				break;
			case 4:
				System.out.println("Postorder traversing");
				tree.postOrder(root);
				break;
			case 5:
				tree.smallestElement(root);
				break;
			case 6:
				tree.largestElement(root);
				break;
			case 7:
				System.out.println("Enter integer element to search : ");
				boolean found = tree.searchElement(new Integer(scan.nextInt()));
				System.out.println(found ? "Found" : "Not Found");
				break;
			default:
				System.out.println("Wrong Entry \n ");
				break;
			}
			System.out.println("\nDo you want to continue (Type y or n) \n");
			ch = scan.next().charAt(0);
		} while (ch == 'Y' || ch == 'y');
		scan.close();

	}

	private void insertANode(int value) {
		Node newNode = new Node(value, null, null);
		if (root == null) {
			root = newNode;
			root.left = null;
			root.right = null;
			return;
		}
		Node current = root;
		while (true) {
			Node parent = current;

			if (value < current.data) {
				current = current.left;
				if (current == null) {
					parent.left = newNode;
					return;
				}
			} else {
				current = current.right;
				if (current == null) {
					parent.right = newNode;
					return;
				}
			}
		}

	}

	private void inOrder(Node p) {
		if (p.left != null)
			inOrder(p.left);
		System.out.println(p.data);
		if (p.right != null)
			inOrder(p.right);
	}

	private void preOrder(Node p) {
		System.out.println(p.data);
		if (p.left != null)
			preOrder(p.left);
		if (p.right != null)
			preOrder(p.right);
	}

	private void postOrder(Node p) {
		if (p.left != null)
			postOrder(p.left);
		if (p.right != null)
			postOrder(p.right);
		System.out.println(p.data);
	}

	private void smallestElement(Node root) {
		Node ptr = root;
		while (ptr.left != null) {
			ptr = ptr.left;
		}
		System.out.println("smallest element is " + ptr.data);
	}

	private void largestElement(Node root) {
		Node ptr = root;
		while (ptr.right != null) {
			ptr = ptr.right;
		}
		System.out.println("Largest element is " + ptr.data);
	}

	private boolean searchElement(int value) {
		Node ptr = root;
		while (true) {
			if (value < ptr.data)
				ptr = ptr.left;
			else if (value > ptr.data)
				ptr = ptr.right;
			else
				return true;

			if (ptr == null)
				return false;
		}

	}
}
