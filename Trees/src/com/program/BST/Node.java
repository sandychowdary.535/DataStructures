package com.program.BST;

public class Node {
	int data;
	Node left;
	Node right;

	public Node(int data, Node left, Node right) {
		super();
		this.data = data;
		this.left = left;
		this.right = right;
	}

}
