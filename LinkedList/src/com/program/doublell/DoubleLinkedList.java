package com.program.doublell;

import java.util.Scanner;

public class DoubleLinkedList {
	protected Node start;
	protected Node end;
	public int size;

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		/* Creating object of class linkedList */
		DoubleLinkedList list = new DoubleLinkedList();
		System.out.println("Singly Linked List Test\n");
		char ch;
		/* Perform list operations */
		do {
			System.out.println("\nSingly Linked List Operations\n");
			System.out.println("1. insert at begining");
			System.out.println("2. insert at end");
			System.out.println("3. insert at position");
			System.out.println("4. delete at position");
			System.out.println("5. check empty");
			System.out.println("6. get size");
			System.out.println("7. display elements");
			System.out.println("8. display backwards");
			int choice = scan.nextInt();
			switch (choice) {
			case 1:
				System.out.println("Enter integer element to insert");
				list.insertAtStart(new Integer(scan.nextInt()).toString());
				break;
			case 2:
				System.out.println("Enter integer element to insert");
				list.insertAtEnd(new Integer(scan.nextInt()).toString());
				break;
			case 3:
				System.out.println("Enter integer element to insert");
				String num = new Integer(scan.nextInt()).toString();
				System.out.println("Enter position");
				int pos = scan.nextInt();
				if (pos <= 1 || pos > list.getSize())
					System.out.println("Invalid position\n");
				else
					list.insertAtPosition(num, pos);
				break;
			case 4:
				System.out.println("Enter position");
				int p = scan.nextInt();
				if (p < 1 || p > list.getSize())
					System.out.println("Invalid position\n");
				else
					list.deleteAtPosition(p);
				break;
			case 5:
				System.out.println("Empty status = " + list.isEmpty());
				break;
			case 6:
				System.out.println("Size = " + list.getSize() + " \n");
				break;
			case 7:
				list.display();
				break;
			case 8:
				list.displayBack();
				break;
			default:
				System.out.println("Wrong Entry \n ");
				break;
			}
			System.out.println("\nDo you want to continue (Type y or n) \n");
			ch = scan.next().charAt(0);
		} while (ch == 'Y' || ch == 'y');
		scan.close();
	}

	private boolean isEmpty() {
		return start == null;
	}

	private void display() {
		System.out.print("\nSingly Linked List = ");
		if (size == 0) {
			System.out.print("empty\n");
			return;
		}
		if (start.next == null) {
			System.out.println(start.data);
			return;
		}
		Node ptr = start;
		System.out.print(start.data + "->");
		ptr = start.next;
		while (ptr.next != null) {
			System.out.print(ptr.data + "->");
			ptr = ptr.next;
		}
		System.out.print(ptr.data + "\n");

	}

	private void displayBack() {
		System.out.print("\nSingly Linked List = ");
		if (size == 0) {
			System.out.print("empty\n");
			return;
		}
		if (start.next == null) {
			System.out.println(start.data);
			return;
		}
		Node ptr = end;
		System.out.print(end.data + "->");
		ptr = end.prev;
		while (ptr.prev != null) {
			System.out.print(ptr.data + "->");
			ptr = ptr.prev;
		}
		System.out.print(ptr.data + "\n");

	}

	private int getSize() {
		return size;
	}

	private void insertAtStart(String val) {
		Node newNode = new Node(val, null, null);
		if (start == null) {
			start = newNode;
			end = start;
		} else {
			newNode.next = start;
			start.prev = newNode;
			start = newNode;
		}
		size++;

	}

	private void insertAtEnd(String val) {
		Node newNode = new Node(val, null, null);
		if (start == null) {
			start = newNode;
			end = start;
		} else {
			end.next = newNode;
			newNode.prev = end;
			end = newNode;
		}
		size++;

	}

	private void insertAtPosition(String val, int pos) {
		Node newNode = new Node(val, null, null);
		Node ptr = start;
		pos = pos - 1;
		for (int i = 1; i < size; i++) {
			if (i == pos) {
				Node tmp = ptr.next;
				ptr.next = newNode;
				newNode.prev = ptr;
				newNode.next = tmp;
				tmp.prev = newNode;
				break;
			}
			ptr = ptr.next;
		}
		size++;

	}

	private void deleteAtPosition(int pos) {
		// removal of first element
		if (pos == 1) {
			start = start.next;
			size--;
			return;
		}
		// removal of last element
		if (pos == size) {
			end = end.prev;
			size--;
			return;
		}
		// removal at specific position
		Node ptr = start;
		pos = pos - 1;
		for (int i = 1; i < size; i++) {
			if (i == pos) {
				Node tmp = ptr.next;
				tmp = tmp.next;
				ptr.next = tmp;
				tmp.prev = ptr;
				break;
			}
			ptr = ptr.next;
		}
		size--;

	}
}
