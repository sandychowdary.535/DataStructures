package com.program.circularsll;

import java.util.Scanner;

public class CircularSingleLinkedList {
	protected Node start;
	protected Node end;
	public int size;

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		/* Creating object of class linkedList */
		CircularSingleLinkedList list = new CircularSingleLinkedList();
		System.out.println("Singly Linked List Test\n");
		char ch;
		/* Perform list operations */
		do {
			System.out.println("\nSingly Linked List Operations\n");
			System.out.println("1. insert at begining");
			System.out.println("2. insert at end");
			System.out.println("3. insert at position");
			System.out.println("4. delete at position");
			System.out.println("5. check empty");
			System.out.println("6. get size");
			int choice = scan.nextInt();
			switch (choice) {
			case 1:
				System.out.println("Enter integer element to insert");
				list.insertAtStart(new Integer(scan.nextInt()).toString());
				break;
			case 2:
				System.out.println("Enter integer element to insert");
				list.insertAtEnd(new Integer(scan.nextInt()).toString());
				break;
			case 3:
				System.out.println("Enter integer element to insert");
				String num = new Integer(scan.nextInt()).toString();
				System.out.println("Enter position");
				int pos = scan.nextInt();
				if (pos <= 1 || pos > list.getSize())
					System.out.println("Invalid position\n");
				else
					list.insertAtPosition(num, pos);
				break;
			case 4:
				System.out.println("Enter position");
				int p = scan.nextInt();
				if (p < 1 || p > list.getSize())
					System.out.println("Invalid position\n");
				else
					list.deleteAtPosition(p);
				break;
			case 5:
				System.out.println("Empty status = " + list.isEmpty());
				break;
			case 6:
				System.out.println("Size = " + list.getSize() + " \n");
				break;
			default:
				System.out.println("Wrong Entry \n ");
				break;
			}
			/* Display List */
			list.display();
			System.out.println("\nDo you want to continue (Type y or n) \n");
			ch = scan.next().charAt(0);
		} while (ch == 'Y' || ch == 'y');
		scan.close();

	}

	public boolean isEmpty() {
		return start == null;
	}

	public int getSize() {
		return size;
	}

	public void insertAtStart(String val) {
		Node newNode = new Node(val, null);
		newNode.link = start;
		if (start == null) {
			start = newNode;
			newNode.link = start;
			end = start;
		} else {
			end.link = newNode;
			start = newNode;
		}
		size++;
	}

	public void insertAtEnd(String val) {
		Node newNode = new Node(val, null);
		newNode.link = start;
		if (start == null) {
			start = newNode;
			newNode.link = start;
			end = start;
		} else {
			end.link = newNode;
			end = newNode;
		}
		size++;
	}

	public void insertAtPosition(String val, int pos) {
		Node newNode = new Node(val, null);
		Node ptr = start;
		pos = pos - 1;
		for (int i = 1; i < size - 1; i++) {
			if (i == pos) {
				Node tmp = ptr.link;
				ptr.link = newNode;
				newNode.link = tmp;
				break;
			}
			ptr = ptr.link;
		}
		size++;
	}

	public void deleteAtPosition(int pos) {
		if (size == 1 && pos == 1) {
			start = null;
			end = null;
			size = 0;
			return;
		}
		if (pos == 1) {
			start = start.link;
			end.link = start;
			size--;
			return;
		}
		if (pos == size) {
			Node s = start;
			Node t = start;
			while (s != end) {
				t = s;
				s = s.link;
			}
			end = t;
			end.link = start;
			size--;
			return;
		}
		Node ptr = start;
		pos = pos - 1;
		for (int i = 1; i < size - 1; i++) {
			if (i == pos) {
				Node tmp = ptr.link;
				tmp = tmp.link;
				ptr.link = tmp;
				break;
			}
			ptr = ptr.link;
		}
		size--;
	}

	/* Function to display contents */
	public void display() {
		System.out.print("\nCircular Singly Linked List = ");
		Node ptr = start;
		if (size == 0) {
			System.out.print("empty\n");
			return;
		}
		if (start.link == start) {
			System.out.print(start.data + "->" + ptr.data + "\n");
			return;
		}
		System.out.print(start.data + "->");
		ptr = start.link;
		while (ptr.link != start) {
			System.out.print(ptr.data + "->");
			ptr = ptr.link;
		}
		System.out.print(ptr.data + "->");
		ptr = ptr.link;
		System.out.print(ptr.data + "\n");
	}
}
