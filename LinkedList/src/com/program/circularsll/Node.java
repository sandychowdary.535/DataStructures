package com.program.circularsll;

public class Node {

	String data;
	Node link;

	public Node(String data, Node link) {
		super();
		this.data = data;
		this.link = link;
	}

}
