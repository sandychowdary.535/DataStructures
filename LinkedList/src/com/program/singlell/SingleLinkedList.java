package com.program.singlell;

import java.util.Scanner;

public class SingleLinkedList {

	protected Node start;
	protected Node end;
	public int size;

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		/* Creating object of class linkedList */
		SingleLinkedList list = new SingleLinkedList();
		System.out.println("Singly Linked List Test\n");
		char ch;
		/* Perform list operations */
		do {
			System.out.println("\nSingly Linked List Operations\n");
			System.out.println("1. insert at begining");
			System.out.println("2. insert at end");
			System.out.println("3. insert at position");
			System.out.println("4. delete at position");
			System.out.println("5. check empty");
			System.out.println("6. get size");
			int choice = scan.nextInt();
			switch (choice) {
			case 1:
				System.out.println("Enter integer element to insert");
				list.insertAtStart(new Integer(scan.nextInt()).toString());
				break;
			case 2:
				System.out.println("Enter integer element to insert");
				list.insertAtEnd(new Integer(scan.nextInt()).toString());
				break;
			case 3:
				System.out.println("Enter integer element to insert");
				String num = new Integer(scan.nextInt()).toString();
				System.out.println("Enter position");
				int pos = scan.nextInt();
				if (pos <= 1 || pos > list.getSize())
					System.out.println("Invalid position\n");
				else
					list.insertAtPosition(num, pos);
				break;
			case 4:
				System.out.println("Enter position");
				int p = scan.nextInt();
				if (p < 1 || p > list.getSize())
					System.out.println("Invalid position\n");
				else
					list.deleteAtPosition(p);
				break;
			case 5:
				System.out.println("Empty status = " + list.isEmpty());
				break;
			case 6:
				System.out.println("Size = " + list.getSize() + " \n");
				break;
			default:
				System.out.println("Wrong Entry \n ");
				break;
			}
			/* Display List */
			list.display();
			System.out.println("\nDo you want to continue (Type y or n) \n");
			ch = scan.next().charAt(0);
		} while (ch == 'Y' || ch == 'y');
		scan.close();
	}

	private void display() {
		System.out.print("\nSingly Linked List = ");
		if (size == 0) {
			System.out.print("empty\n");
			return;
		}
		if (start.link == null) {
			System.out.println(start.data);
			return;
		}
		Node ptr = start;
		System.out.print(start.data + "->");
		ptr = start.link;
		while (ptr.link != null) {
			System.out.print(ptr.data + "->");
			ptr = ptr.link;
		}
		System.out.print(ptr.data + "\n");
	}

	private boolean isEmpty() {
		return start == null;
	}

	private int getSize() {
		return size;
	}

	private void deleteAtPosition(int pos) {
		if (pos == 1) {
			start = start.link;
			size--;
			return;
		}
		if (pos == size) {
			Node s = start;
			Node t = start;
			while (s != end) {
				t = s;
				s = s.link;
			}
			end = t;
			end.link = null;
			size--;
			return;
		}
		Node ptr = start;
		pos = pos - 1;
		for (int i = 1; i < size - 1; i++) {
			if (i == pos) {
				Node tmp = ptr.link;
				tmp = tmp.link;
				ptr.link=tmp;
				break;
			}
			ptr = ptr.link;
		}
		size--;

	}

	private void insertAtPosition(String val, int pos) {
		Node newnode = new Node(val, null);
		Node ptr = start;
		pos = pos - 1;
		for (int i = 1; i < size; i++) {
			if (i == pos) {
				Node tmp = ptr.link;
				ptr.link = newnode;
				newnode.link = tmp;
				break;
			}
			ptr = ptr.link;
		}
		size++;

	}

	private void insertAtEnd(String val) {
		Node newNode = new Node(val, null);
		size++;
		if (start == null) {
			start = newNode;
			end = start;
		} else {
			end.link = newNode;
			end = newNode;
		}

	}

	private void insertAtStart(String val) {
		Node newNode = new Node(val, null);
		size++;
		if (start == null) {
			start = newNode;
			end = start;
		} else {
			newNode.link = start;
			start = newNode;
		}

	}
}
